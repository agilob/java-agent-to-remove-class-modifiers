public class Main {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> clazz = Class.forName("java/nio/channels/Channels".replace("/", "."));
        final int modifiers = clazz.getModifiers();
        System.out.println("Happy days for modifiers: " + modifiers);
//        new java.nio.channels.Channels();
    }
}
