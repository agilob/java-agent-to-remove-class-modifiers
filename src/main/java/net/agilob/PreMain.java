package net.agilob;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.Modifier;
import javassist.NotFoundException;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

public class PreMain {

    public static void premain(String agentArgs, Instrumentation inst) {
        inst.addTransformer(new ClassFileTransformer() {

            @Override
            public byte[] transform(ClassLoader l, String name, Class c,
                                    ProtectionDomain d, byte[] b)
                    throws IllegalClassFormatException {

//                new java.nio.channels.Channels();
                if (name.equals("java/nio/channels/Channels")) {

                    ClassPool classPool = ClassPool.getDefault();
                    final CtClass ctClass;
                    try {
                        ctClass = classPool.get(name.replace("/", "."));
                    } catch (NotFoundException e) {
                        throw new RuntimeException(e);
                    }
                    int modifiers = ctClass.getModifiers();
                    if (Modifier.isFinal(modifiers)) {

                        System.out.println(name + " modifiers: " + modifiers);
                        ctClass.setModifiers(ctClass.getModifiers() & -Modifier.PUBLIC);
                        ctClass.setModifiers(ctClass.getModifiers() & -Modifier.FINAL);

                        try {
                            System.out.println("removed modifiers");
                            return ctClass.toBytecode();
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                }
                return b;
            }
        });
    }

}
